export default {
  // Target Deployment
  target: 'server',

  // SSR
  ssr: false,
  loading: {
    color: 'blue',	
// <-- color
    height: '8px'	
// <-- height
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'SISKHA',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap'
      }
    ],
    script: [{
      src: '/js/jquery/jquery.min.js'
    },
    {
      src: '/js/bootstrap/js/bootstrap.bundle.min.js'
    },
    {
      src: '/js/adminlte.min.js'
    },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/fontawesome-free/css/all.min.css',
    '@/assets/css/adminlte.min.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vue-star-rating.js', mode: 'client' },
    { src: '~/plugins/chart.js', mode: 'client' },
    { src: '~/plugins/mixin.js', mode: 'client' },
    { src: '~/plugins/config/coreui.config.js', mode: 'client' },
    { src: '~/plugins/helpers/datemoment.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    //https://auth.nuxtjs.org/
    '@nuxtjs/auth-next',
    //https://github.com/maulayyacyber/nuxt-vue-multiselect
    'nuxt-multiselect',
    //https://github.com/avil13/vue-sweetalert2
    'vue-sweetalert2/nuxt',
  ],

  auth: {
    strategies: {
      local: {
        token: {
          property: 'data.token',
          maxAge: 10080 * 60,
          required: true,
          type: 'Bearer'
        },
        user: {
          property: '',
          // autoFetch: true
        },
        endpoints: {
          login: {
            url: '/login',
            method: 'post',
            propertyName: 'data.token'

          },
          logout: {
            url: '/logout',
            method: 'post'
          },
          user: {
            url: '/profile',
            method: 'get'
          }
        }
      }
    },
    localStorage: true,
    redirect: {
      login: '/login',
      logout: '/login',
      callback: '/login',
      home: '/admin/dashboard'
    }
  },
  pwa: {
    manifest: {
      lang: 'en',
    },
  },
  router: {
    // middleware: ['auth']
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    // baseURL: 'https://siska-api.loccoprima.com/api',
    baseURL: 'https://be-template.test/api',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
