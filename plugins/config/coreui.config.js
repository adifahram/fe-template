import Vue from 'vue';

import vSelect from "vue-select";
import 'vue-select/dist/vue-select.css';
// date picker
import DatePicker from 'vue2-datepicker'
import 'vue2-datepicker/index.css';

// integration components
Vue.component('vSelect', vSelect);
Vue.component('date-picker', DatePicker);


export default ({
  app,
}) => {
  
}
