import moment from "moment";

export default ({
  app
}, inject) => {
  inject("df", (string) => {
    return moment(string).format("YYYY-MM-DDThh:mm:ss");
  });
  inject("momentsave", (string) => {
    return moment(string).format("YYYY-MM-DD hh:mm:ss");
  });
  inject("formatDatetime", (string) => {
    // 24 hours format
    return (!string) ? '-' : moment(string).format("YYYY-MM-DD HH:mm:ss");
  });
  inject("rdf", (string) => {
    var dateobj = new Date(string);

    // Contents of above date object is
    // converted into a string using toISOString() method.
    var date = dateobj.toISOString();
    return date;
  });
  inject("numberFormat", (number) => {
    // format number
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  });
};
