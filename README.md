```
  _____                _                 _ 
 |  ___| __ ___  _ __ | |_ ___ _ __   __| |
 | |_ | '__/ _ \| '_ \| __/ _ \ '_ \ / _` |
 |  _|| | | (_) | | | | ||  __/ | | | (_| |
 |_|  |_|  \___/|_| |_|\__\___|_| |_|\__,_|
                                                                      
```

## Cara Clone
```bash
$ git clone https://gitlab.com/$repo_project_baru
$ cd $repo_project_baru
$ git checkout (nama_programmer)
```

Copy dan Ganti password yang ditemukan di `.env.example`

...coding
#### Review /Merge Branch

```bash
$ git add .
$ git commit -m "penjelasan detil"
$ git push origin (nama_programmer)
```


## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
